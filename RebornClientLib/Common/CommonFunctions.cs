﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace GISClientLib.Common
{
    public static class CommonFunctions
    {
        private static string mRebornBaseAddress = "", mServiceHostingType = "";

        public static string getRebornBaseAddress()
        {
            if (mRebornBaseAddress.Equals(""))
            {
                mRebornBaseAddress = ConfigurationManager.AppSettings["RebornBaseAddress"].ToString();

                if(!mRebornBaseAddress.EndsWith("/"))
                    mRebornBaseAddress += "/";

                return mRebornBaseAddress;
            }
            else
            {
                return mRebornBaseAddress;

            }
        
        }

        public static string getRebornEndpointAddress(string endpoint)
        {
            try
            {
                if(mServiceHostingType.Equals(""))
                    mServiceHostingType = ConfigurationManager.AppSettings["ServiceHostingType"].ToString();

                if (mServiceHostingType.Equals("Standalone"))
                {
                    return endpoint;
                }
                else if (mServiceHostingType.Equals("IIS"))
                {
                    return endpoint += ".svc";
                }
                else 
                {
                    return endpoint;
                }

            }
            catch
            {
                return endpoint;
            }

        }

        public static string getCurrentDecimalSeparator()
        {
            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
            return ci.NumberFormat.CurrencyDecimalSeparator;
        }

        public static string convertToRightDecimnalSeperator(string value)
        {
            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
            string sep = ci.NumberFormat.CurrencyDecimalSeparator;

            if (sep == ".")
                return value.Replace(",", ".");
            else
                return value.Replace(".", ",");

        }

    }
}