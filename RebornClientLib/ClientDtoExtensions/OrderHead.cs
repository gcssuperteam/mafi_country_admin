﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GIS_Dto
{
    public partial class OrderHead
    {
        private bool? _IsWholeOrderDeliverable = null;
        private bool? _DoesOrderContainsMeasure = null;

        public bool? IsWholeOrderDeliverable
        {
            get { return _IsWholeOrderDeliverable; }
            set
            {
                if (_IsWholeOrderDeliverable == value) return;
                    _IsWholeOrderDeliverable = value;
                
            }
        }

        public bool? DoesOrderContainsMeasure
        {
            get { return _DoesOrderContainsMeasure; }
            set
            {
                if (_DoesOrderContainsMeasure == null)
                {
                    _DoesOrderContainsMeasure = value;
                }
            }
        }
        public int NonDeliverableRows { get; set; }
        public List<string> TypeOfMeasure { get; set; }
        public string CustomerCategoryName { get; set; }
        public string CustomerCategoryCode { get; set; }
        public bool IsMeasureDone { get; set; }
        public int DeliverableRowsCount { get; set; }
        public bool Relations { get; set; }
        public decimal NoOfPieces { get; set; }
        public string CustomerCode3 { get; set; }
        public bool IsRowsWithinDates { get; set; }
        public bool isExport { get; set; }
        public bool isWorkOrder { get; set; }
        public bool isLargeOrder { get; set; }
        public bool IncludeInPickList { get; set; }
        public bool isPrioOrder { get; set; }
        public string DeliverWayId { get; set; }
        public bool HasAdditional { get; set; }     //Fattades, så tillagd 190502
    }

}
