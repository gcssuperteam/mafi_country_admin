﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
//using System.Net.Http;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;

namespace GISClientLib.Models
{
    public class HistoryModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public HistoryModel(string token, bool use_token_call) : base(token, use_token_call)
        {
            if (!string.IsNullOrEmpty(token))
            {
                base.Token = token;
            }
        }


        public List<CustomerHistory> GetHistoryOnCustomerList(string customer_from, string customer_to, string date_from, string date_to)
        {
            string rest = "";

            List<CustomerHistory> lst = new List<CustomerHistory>();

            try
            {
                rest = getAddress("/REST/GetHistoryOnCustomerList") + @"/" + customer_from + @"/" + customer_to + @"/" + date_from + @"/" + date_to;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Timeout = 3600000;

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<CustomerHistory>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("HistorySvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("HistorySvc") + rest_function;
        }

    }
}
