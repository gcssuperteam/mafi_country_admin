﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;

namespace GISClientLib.Models
{
    public class LogtradeModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LogtradeModel(string token, bool use_token_call) : base(token, use_token_call)
        {
        }

        public string TestLogtradeConnection(string token)
        {
            string rest = "", result = "";

            try
            {
                rest = getAddress("/REST/TestLogtradeConnection");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<string>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public LogtradeForwarder GetForwarder(string token, string forwardercode, bool new_read)
        {
            string rest = "";
            LogtradeForwarder forwarder = new LogtradeForwarder();

            try
            {
                rest = getAddress("/REST/GetForwarder") + "/" + forwardercode + "/" + new_read.ToString();

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                forwarder = JsonConvert.DeserializeObject<LogtradeForwarder>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return forwarder;
        }

        public List<LogtradeForwarder> GetForwarderList(string token, bool new_read)
        {
            string rest = "";
            List<LogtradeForwarder> lst = new List<LogtradeForwarder>();

            try
            {
                rest = getAddress("/REST/GetForwarderList") + "/" + new_read.ToString();

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeForwarder>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradePackageType> GetProductPackageTypeList(string token, string productid, bool custom)
        {
            string rest = "";
            List<LogtradePackageType> lst = new List<LogtradePackageType>();

            try
            {
                rest = getAddress("/REST/GetProductPackageTypeList") + "/" + productid + "/" + custom.ToString();

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradePackageType>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradeShipmentService> GetShipmentServiceList(string token, string productid)
        {
            string rest = "";
            List<LogtradeShipmentService> lst = new List<LogtradeShipmentService>();

            try
            {
                rest = getAddress("/REST/GetShipmentServiceList") + "/" + productid ;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeShipmentService>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradeTermOfDelivery> GetTermsOfDeliveryList(string token, string productid)
        {
            string rest = "";
            List<LogtradeTermOfDelivery> lst = new List<LogtradeTermOfDelivery>();

            try
            {
                rest = getAddress("/REST/GetTermsOfDeliveryList") + "/" + productid ;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeTermOfDelivery>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public List<LogtradeSenderAddress> GetSenderAddressList(string token)
        {
            string rest = "";
            List<LogtradeSenderAddress> lst = new List<LogtradeSenderAddress>();

            try
            {
                rest = getAddress("/REST/GetSenderAddressList");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<LogtradeSenderAddress>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public LogtradeShipmentResponse CreateShipment(string token, LogtradeShipment shipment)
        {
            string rest = "";
            LogtradeShipmentResponse lst = new LogtradeShipmentResponse();

            try
            {
                rest = getAddress("/REST/CreateShipment");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(shipment));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();

                    lst = JsonConvert.DeserializeObject<LogtradeShipmentResponse>(result);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public LogtradePriceQuestionResponse GetShipmentPrice(string token, LogtradeShipment shipment)
        {
            string rest = "";
            LogtradePriceQuestionResponse lst = new LogtradePriceQuestionResponse();

            try
            {
                rest = getAddress("/REST/GetShipmentPrice");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(shipment));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();

                    lst = JsonConvert.DeserializeObject<LogtradePriceQuestionResponse>(result);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("LogtradeSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("LogtradeSvc") + rest_function;
        }
    }
}
