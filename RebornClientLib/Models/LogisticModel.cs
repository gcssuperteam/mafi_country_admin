﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using GIS_Dto;

namespace GISClientLib.Models
{
    public class LogisticModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LogisticModel(string token, bool use_token_call) : base(token, use_token_call)
        {
            if (!string.IsNullOrEmpty(token))
            {
                base.Token = token;
            }
        }

        public List<OrderHead> GetOrderList(OrderListParam param)
        {
            List<OrderHead> result = new List<OrderHead>();

            string rest = "";
            try
            {
                rest = getAddress("/REST/GetOrderList");

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<List<OrderHead>>(json);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public List<OrderHead> GetOrderListWithAdditionalOrder(PickListParam param)
        {
            List<OrderHead> result = new List<OrderHead>();

            string rest = "";
            try
            {
                rest = getAddress("/REST/GetOrderPickListWithAdditionalOrder");

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string json = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<List<OrderHead>>(json);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("LogisticSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("LogisticSvc") + rest_function;

        }
    }
}
