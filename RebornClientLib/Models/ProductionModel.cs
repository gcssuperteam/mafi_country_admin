﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;

namespace GISClientLib.Models
{
    public class ProductionModel:BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ProductionModel(string token, bool use_token_call) : base(token, use_token_call)
        {

        }
        public MaterialOrderHead GetMaterialOrder( string onr, string rdc)
        {
            string rest = "";
            MaterialOrderHead result = new MaterialOrderHead();
            rest = getAddress("/REST/GetMaterialOrder") + "/" + onr +"/"+rdc;
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<MaterialOrderHead>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;

        }
        public List<MaterialOrderHead> GetMaterialOrderList(MaterialOrderListParam param)
        {
            string rest = "";
            List<MaterialOrderHead> result = new List<MaterialOrderHead>();

            try
            {
                rest = getAddress("ProductionSvc") + "/REST/GetMaterialOrderList" + "/" + Token;

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }
                using (var response = request.GetResponse())
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string resultString = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<List<MaterialOrderHead>>(resultString);
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public ProductionOrderHead GetProductionOrder(string onr, string rdc)
        {
            string rest = "";
            ProductionOrderHead result = new ProductionOrderHead();

            rest = getAddress("/REST/GetProductionOrder") + "/" + onr + "/" + rdc;
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<ProductionOrderHead>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }



            return result;
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("ProductionSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("ProductionSvc") + rest_function;
        }
    }
}
