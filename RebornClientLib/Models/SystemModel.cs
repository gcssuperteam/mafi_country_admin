﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;

namespace GISClientLib.Models
{
    public class SystemModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SystemModel(string token, bool use_token_call) : base(token, use_token_call)
        {
        }

        public UserConfiguration GetCurrentConfiguration()
        {
            string rest = "";
            UserConfiguration uc = new UserConfiguration();

            rest = getAddress("/REST/GetCurrentConfiguration/");
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                uc = JsonConvert.DeserializeObject<UserConfiguration>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return uc;
        }


        public SellerDTO GetSeller(string id)
        {
            string rest = "";
            SellerDTO seller = new SellerDTO();

            rest = getAddress("/REST/GetSeller") +@"/"+id;
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                seller = JsonConvert.DeserializeObject<SellerDTO>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return seller;
        }

        public UserErpInfo GetUserErpInfo(string id)
        {
            string rest = "";
            UserErpInfo result = new UserErpInfo();

            rest = getAddress("/REST/GetUserErpInfo") + @"/" + id;
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<UserErpInfo>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("RebornSystem") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("RebornSystem") + rest_function;
        }

    }
}
