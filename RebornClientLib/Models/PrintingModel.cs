﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
//using System.Net.Http;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;


namespace GISClientLib.Models
{
    public class PrintingModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PrintingModel(string token, bool use_token_call) : base(token, use_token_call)
        {
        }

        public bool Print(ERPReport report)
        {
            string rest = "";

            string json = JsonConvert.SerializeObject(report);
            mLog.Debug("CALL TO Print with JSON: " + json);

            try
            {
                rest = getAddress("/REST/Print");

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(report));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();

                    return Convert.ToBoolean(result);
                    // do something with the results

                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return false;
            }
        }

        public byte[] PrintPdfToClient(ERPReport report)
        {

            string rest = "";
            var result = default(byte[]);
            try
            {
                rest = getAddress("/REST/PrintPdfToClient");

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(report));
                }

                using (var response = request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (MemoryStream ms = new MemoryStream())
                        {
                            int count = 0;
                            do
                            {
                                byte[] buf = new byte[1024];
                                count = stream.Read(buf, 0, 1024);
                                ms.Write(buf, 0, count);
                            } while (stream.CanRead && count > 0);
                            result = ms.ToArray();

                            mLog.Debug("Size of result: " + result.Length);
                    return result;
                    // do something with the results
                }
                
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return null;
            }
        }


        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("PrintingSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("PrintingSvc") + rest_function;
        }
    }
}
