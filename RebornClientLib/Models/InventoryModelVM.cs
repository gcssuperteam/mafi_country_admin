﻿using GIS_Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GISClientLib.Models
{
    public class InventoryModelVM : BaseModel
    {
        public Product product { get; set; }
        public InventoryResult iResult { get; set; }

    }
}
