﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;

namespace GISClientLib.Models
{
    public class WarehouseModel: BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public WarehouseModel(string token, bool use_token_call) : base(token, use_token_call)
        {
        }

        public List<WarehouseNumber> GetProductWarehouseList(string productno)
        {
            string rest = "";
            List<WarehouseNumber> lst = new List<WarehouseNumber>();

            try
            {
                rest = getAddress("/REST/GetProductWarehouseList") + "/" + productno;
                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<WarehouseNumber>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;

        }

        public UpdateResult AddWarehouse(WarehouseNumber warehouse)
        {
            string rest = "";
            UpdateResult result = new UpdateResult();
            
            try
            {

                rest = getAddress("/REST/AddWarehouse");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(warehouse));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<UpdateResult>(response);

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                result.Succeeded = false;
                result.InternalMoveMessage = e.Message;
            }
            return result;
        }

        public UpdateResult UpdateWarehouse(WarehouseNumber warehouse)
        {
            string rest = "";
            UpdateResult result = new UpdateResult();

            try
            {

                rest = getAddress("/REST/UpdateWarehouse");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(warehouse));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<UpdateResult>(response);

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                result.Succeeded = false;
                result.InternalMoveMessage = e.Message;
            }
            return result;
        }

        public UpdateResult DeleteWarehouse(WarehouseNumber warehouse)
        {
            string rest = "";
            UpdateResult result = new UpdateResult();

            try
            {

                rest = getAddress("/REST/DeleteWarehouse");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(warehouse));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<UpdateResult>(response);

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                result.Succeeded = false;
                result.InternalMoveMessage = e.Message;
            }
            return result;
        }

        public MoveResult MoveProductBetweenWarehouses(string productno, string from, string to, decimal amount)
        {
            string rest = "";
            MoveResult result = new MoveResult();

            try
            {

                rest = getAddress("/REST/MoveProduct")+"/"+productno+"/"+from +"/"+to+"/"+amount;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);


                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<MoveResult>(response);

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                result.Succeeded = false;
                result.InternalMoveMessage = e.Message;
            }
            return result;
        }

        public MoveResult MoveProductBetweenWarehouses(MoveParam param)
        {
            string rest = "";
            MoveResult result = new MoveResult();

            try
            {

                rest = getAddress("/REST/MoveProductLocation");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();
                
                try
                {
                    result = JsonConvert.DeserializeObject<MoveResult>(response);
                }
                catch (Exception e)
                {
                    mLog.Error("Error parsing json in MoveProductBetweenWarehouses ", e);
                    result.Succeeded = true;
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                result.Succeeded = false;
                result.InternalMoveMessage = e.Message;
            }
            return result;
        }


        public WarehouseBalancingResult WarehouseBalancing(string token, WarehouseBalancingParam param)
        {
            string rest = "";

            try
            {
                WarehouseBalancingResult result = new WarehouseBalancingResult();
                rest = getAddress("/REST/WarehouseBalancing");

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = JsonConvert.DeserializeObject<WarehouseBalancingResult>(reader.ReadToEnd());
                }

                return result;
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return null;
            }
        }


        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("WarehouseSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("WarehouseSvc") + rest_function;
        }
    }
}
