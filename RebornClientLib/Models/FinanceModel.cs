﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Mvc;
using GISClientLib.Models;
using GIS_Dto;
using GISClientLib;
//using GDN.Models;
using System.Configuration;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;

namespace GISClientLib.Models
{
    public class FinanceModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public FinanceModel()
        {

        }

        public FinanceModel(string token, bool use_token_call) : base(token, use_token_call)
        {
        }

        public List<AccountTransaction> GetAccountTransactionList(string fiscalyearid, string period, string account, bool includeledger)
        {
            string rest = "";
            List<AccountTransaction> lst = new List<AccountTransaction>();

            try
            {
                rest = Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("FinanceSvc") + "/REST/GetAccountTransactionList/" + Token + "/" + fiscalyearid + "/" + period + "/" + account + "/" + includeledger.ToString();

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<AccountTransaction>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public IncomeStatement GetIncomeStatement(string fiscalyearid, string periodfrom, string periodto)
        {
            string rest = "";
            IncomeStatement result = new IncomeStatement();

            try
            {
                rest = Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("FinanceSvc") + "/REST/GetIncomeStatement/" + Token + "/" + fiscalyearid + "/" + periodfrom + "/" + periodto;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<IncomeStatement>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

    }
}
