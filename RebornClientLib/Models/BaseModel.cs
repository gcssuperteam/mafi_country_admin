﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GISClientLib.Models
{
    public abstract class BaseModel
    {
        private string mToken = "*";
        private bool mUseTokenCall = false;

        public BaseModel()
        {
            try
            {
                mToken = ConfigurationManager.AppSettings["StaticToken"].ToString();
                mUseTokenCall = true;
            }
            catch
            {
                mUseTokenCall = false;
            }
        }

        public BaseModel(string token, bool use_token_call)
        {
            if(string.IsNullOrEmpty(token))
            {
                try
                {
                    mToken = ConfigurationManager.AppSettings["StaticToken"].ToString();
                    mUseTokenCall = true;
                }
                catch
                {
                    mUseTokenCall = false;
                }
            }
            else
            {
                mUseTokenCall = use_token_call;
                Token = token;
            }

        }

        public bool UseTokenCall
        {
            get
            {
                return mUseTokenCall;
            }

            set
            {
                mUseTokenCall = value;
            }
        }

        public string Token
        {
            get
            {
                return mToken;
            }

            set
            {
                if(!value.Equals("")) 
                {
                    mToken = value;
                }
            }
        }

    }
}
