﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using GIS_Dto;
using GISClientLib;

namespace GISClientLib.Models
{
    public class BaseDataModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public BaseDataModel(string token, bool use_token_call) : base(token, use_token_call)
        {
        }

        public List<BaseTable> GetBaseTableList(string type)
        {
            string rest = "";
            List<BaseTable> lst = new List<BaseTable>();

            try
            {
                rest = getAddress("/REST/GetBaseTableList") + "/" + type;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                lst = JsonConvert.DeserializeObject<List<BaseTable>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public GIS_Dto.BaseTable GetBaseTable(string type, string key)
        {
            string rest = "";
            GIS_Dto.BaseTable bt = new GIS_Dto.BaseTable();

            if (string.IsNullOrEmpty(type) || string.IsNullOrEmpty(key))
                return null;

            try
            {
                rest = getAddress("/REST/GetBaseTable") + "/" + type + "/" + key;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                //                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                bt = JsonConvert.DeserializeObject<GIS_Dto.BaseTable>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return null;
            }

            return bt;
        }

        public string AddBaseTable(BaseTable bd)
        {
            string rest = "";

            try
            {
                rest = getAddress("/REST/AddBaseTable");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(bd));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
                return "false";
            }
            return "true";
        }

        public void UpdateBaseTable(BaseTable bd)
        {
            string rest = "";

            try
            {
                rest = getAddress("/REST/UpdateBaseTable");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(bd));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }

        public void DeleteBaseTable(BaseTable bd)
        {
            string rest = "";

            try
            {
                rest = getAddress("/REST/DeleteBaseTable");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(bd));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("BaseDataSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("BaseDataSvc") + rest_function;
        }
    }
}
