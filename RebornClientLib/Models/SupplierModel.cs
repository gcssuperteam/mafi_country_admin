﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;


namespace GISClientLib.Models
{
    public class SupplierModel: BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SupplierModel(string token, bool use_token_call) : base(token, use_token_call)
        {
        }
        public Supplier GetSupplierById(string id)
        {
            string rest = "";
            Supplier supplier = new Supplier();

            rest = getAddress("/REST/GetSupplier") + "/" + id;
            mLog.Debug("CALL TO: " + rest);

            try
            {
                WebRequest request = WebRequest.Create(rest);

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                supplier = JsonConvert.DeserializeObject<Supplier>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return supplier;
        }

        public List<Supplier> getSupplierList(SupplierListParam param)
        {
            string rest = "";
            List<Supplier> supplierList = new List<Supplier>();

            try
            {
                rest = getAddress("/REST/GetSupplierList");
                mLog.Debug("CALL TO: " + rest);

                //var request = WebRequest.Create(rest);
                //request.Method = "POST";
                //request.ContentType = "application/json; charset=utf-8";

                //if (!string.IsNullOrEmpty(Token))
                //{
                //    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                //    request.Headers.Add("Authorization", "Basic " + encoded);
                //}

                //using (var writer = new StreamWriter(request.GetRequestStream()))
                //{
                //    writer.Write(JsonConvert.SerializeObject(param));
                //}

                //using (var response = request.GetResponse())

                //using (var reader = new StreamReader(response.GetResponseStream()))
                //{
                //    supplierList = JsonConvert.DeserializeObject<List<Supplier>>(response);
                //}

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        string result = reader.ReadToEnd();
                        supplierList = JsonConvert.DeserializeObject<List<Supplier>>(result);
                    }
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return supplierList;
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("SupplierSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("SupplierSvc") + rest_function;
        }

    }
}
