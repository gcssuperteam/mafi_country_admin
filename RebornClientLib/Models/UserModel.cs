﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;

namespace GISClientLib.Models
{
    public class UserModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public UserModel(string token, bool use_token_call) : base(token, use_token_call)
        {
            if (!string.IsNullOrEmpty(token))
            {
                base.Token = token;
            }
        }

        public string Login(string userid, string password)
        {
            string rest = "";
            string token = "";
            

            try
            {

                if (!string.IsNullOrEmpty(userid) && !string.IsNullOrEmpty(password))
                {
                    rest = Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("Login") + "/REST/LoginToken/" +Token+"/"+ userid + "/" + password;
                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);

                    if (!string.IsNullOrEmpty(Token))
                    {
                        String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                        request.Headers.Add("Authorization", "Basic " + encoded);
                    }

                    WebResponse ws = request.GetResponse();
                    Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    try
                    {
                        token = response.ToString();
                    }
                    catch (Exception e)
                    {
                        mLog.Error("Login failed: " + rest, e);
                    }
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return token;
        }

        public string Logoff(string userid)
        {
            string rest = "";
            string token = "";

            try
            {

                if (!string.IsNullOrEmpty(userid))
                {
                    rest = Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("Login") + "/REST/LogOffToken/" + userid;
                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);

                    if (!string.IsNullOrEmpty(Token))
                    {
                        String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                        request.Headers.Add("Authorization", "Basic " + encoded);
                    }

                    WebResponse ws = request.GetResponse();
                    Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    try
                    {
                        token = response.ToString();
                    }
                    catch (Exception e)
                    {
                        mLog.Error("Logoff failed: " + rest, e);
                    }
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return token;
        }
        public User GetUser(string uid, string username)
        {
            string rest = "";
            User user = null;

            try
            {

                if (!string.IsNullOrEmpty(username))
                {
                    rest = getAddress("/REST/GetUser") + "/"+ username;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);

                    if (!string.IsNullOrEmpty(Token))
                    {
                        String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(uid + ":" + "password"));
                        request.Headers.Add("Authorization", "Basic " + encoded);
                    }

                    WebResponse ws = request.GetResponse();
                    Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    try
                    {
                        user = JsonConvert.DeserializeObject<User>(response);
                    }
                    catch (Exception e)
                    {
                        mLog.Error("Login failed: " + rest, e);
                    }
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return user;
        }

        private string getAddress(string rest_function)
        {

            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("UserSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("UserSvc") + rest_function;
        }

    }
}
