﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
//using System.Net.Http;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;

namespace GISClientLib.Models.v2
{
    public class OrderModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public OrderModel(string token, bool use_token_call) : base(token, use_token_call)
        {
            if(!string.IsNullOrEmpty(token))
            {
                base.Token = token;
            }
        }

        public OrderResult<OrderHead> AddOrder(OrderHead oh)
        {
            OrderResult<OrderHead> result = new OrderResult<OrderHead>();
            string rest = "";
            try
            {
                rest = getAddress("/REST/AddOrder");

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(oh));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string sres = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<OrderResult<OrderHead>>(sres);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("v2/OrderSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("v2/OrderSvc") + rest_function;
        }

    }
}
