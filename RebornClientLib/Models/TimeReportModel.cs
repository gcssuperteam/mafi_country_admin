﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;

namespace GISClientLib.Models
{
    public class TimeReportModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public TimeReportModel(string token, bool use_token_call) : base(token, use_token_call)
        {

        }

        public List<TimeReport> GetTimeReportList(TimeReportParam param)
        {
            string rest = "";
            List<TimeReport> result = new List<TimeReport>();

            try
            {
                rest = getAddress("/REST/GetTimeReportList");

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }
                using (var response = request.GetResponse())
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string resultString = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<List<TimeReport>>(resultString);
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
            return result;
        }

        public TimeReportStartOrderResult StartOrder(TimeReportStartOrderParam param)
        {
            string rest = "";
            TimeReportStartOrderResult result = new TimeReportStartOrderResult();

            try
            {
                rest = getAddress("/REST/GetTimeReportList");

                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }
                using (var response = request.GetResponse())
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string resultString = reader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<TimeReportStartOrderResult>(resultString);
                    reader.Close();
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
            return result;
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("TimeReportSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("TimeReportSvc") + rest_function;
        }
    }
}