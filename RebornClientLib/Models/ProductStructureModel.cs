﻿using GIS_Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GISClientLib.Models
{
    public class ProductStructureModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       
        public ProductStructureModel(string token, bool use_token_call) : base(token, use_token_call)
        {

        }

        public SingleResult<bool> RemoveStructure(string token, ProductStructureParam param)
        {
            string rest = "";
            SingleResult<bool> result = new SingleResult<bool>();
            try
            {

                rest = getAddress("/REST/RemoveStructure");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = JsonConvert.DeserializeObject<SingleResult<bool>>(reader.ReadToEnd());
                }


            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }
        public SingleResult<bool> DeleteCompleteStructure(string token, ProductStructureParam param)
        {
            string rest = "";
            SingleResult<bool> result = new SingleResult<bool>();
            try
            {

                rest = getAddress("/REST/DeleteCompleteStructure");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = JsonConvert.DeserializeObject<SingleResult<bool>>(reader.ReadToEnd());
                }


            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public SingleResult<bool> CreateStructure(string token, List<ProductStructureParam> param)
        {
            string rest = "";
            SingleResult<bool> result = new SingleResult<bool>();
            try
            {

                rest = getAddress("/REST/CreateStructure");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = JsonConvert.DeserializeObject<SingleResult<bool>>(reader.ReadToEnd());
                }


            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }
        public SingleResult<bool> AddStructure(string token, ProductStructureParam param)
        {
            string rest = "";
            SingleResult<bool> result = new SingleResult<bool>();
            try
            {

                rest = getAddress("/REST/AddStructure");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    result = JsonConvert.DeserializeObject<SingleResult<bool>>(reader.ReadToEnd());
                }


            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result;
        }

        public List<ProductStructure> GetStructure(string token, string parentproduct)
        {
            string rest = "";
            List<ProductStructure> lst = new List<ProductStructure>();

            try
            {

                rest = getAddress("/REST/GetStructure");
                rest += "/" + parentproduct;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "GET";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    lst = JsonConvert.DeserializeObject<List<ProductStructure>>(reader.ReadToEnd());
                }


            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }




        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("ProductStructureSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("ProductStructureSvc") + rest_function;
        }
        private string getGenericSvcAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("GenericTableSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("GenericTableSvc") + rest_function;
        }

    }

   
    
}
