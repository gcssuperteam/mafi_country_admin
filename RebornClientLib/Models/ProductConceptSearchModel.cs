﻿using GIS_Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GISClientLib.Models 
{
    public class ProductConceptSearchModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string restSearch, restAdd, restDelete;
        public ProductConceptSearchModel(string token, bool use_token_call) : base(token, use_token_call)
        {
           
        }

        public List<Product> ConceptSearch(string token, ConceptSearchParam param)
        {
            //string rest = "";
            if (string.IsNullOrWhiteSpace(restSearch))
            {
                restSearch = getAddress("/REST/ConceptSearch");
            }
            List<Product> lst = new List<Product>();

            try
            {

                //rest = getAddress("/REST/ConceptSearch");

                mLog.Debug("CALL TO: " + restSearch);

                WebRequest request = WebRequest.Create(restSearch);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    lst = JsonConvert.DeserializeObject<List<Product>>(reader.ReadToEnd());
                }

              
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + restSearch, e);
            }

            return lst;
        }

        public List<ProductConcept> GetProductConceptList(string token, ProductConceptListParam param)
        {
            //string rest = "";
            if (string.IsNullOrWhiteSpace(restSearch))
            {
                restSearch = getAddress("/REST/GetProductConceptList");
            }
            List<ProductConcept> lst = new List<ProductConcept>();

            try
            {

                //rest = getAddress("/REST/ConceptSearch");

                mLog.Debug("CALL TO: " + restSearch);

                WebRequest request = WebRequest.Create(restSearch);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    lst = JsonConvert.DeserializeObject<List<ProductConcept>>(reader.ReadToEnd());
                }


            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + restSearch, e);
            }

            return lst;
        }


        public bool AddConcept(string token, ProductConcept param)
        {
            // string rest = "";
            if (string.IsNullOrWhiteSpace(restAdd))
            {
                restAdd = getAddress("/REST/AddConcept");
            }
            
            try
            {

               // rest = getAddress("/REST/AddConcept");

                mLog.Debug("CALL TO: " + restAdd);

                var request = WebRequest.Create(restAdd);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    if (result.Equals("true"))
                        return true;
                    else
                        return false;
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + restAdd, e);
                return false;
            }
        }

        public bool AddConceptList(string token, List<ProductConcept> paramList)
        {
            // string rest = "";
            if (string.IsNullOrWhiteSpace(restAdd))
            {
                restAdd = getAddress("/REST/AddConceptList");
            }

            try
            {
                mLog.Debug("CALL TO: " + restAdd);

                var request = WebRequest.Create(restAdd);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(paramList));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    if (result.Equals("true"))
                        return true;
                    else
                        return false;
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + restAdd, e);
                return false;
            }
        }

        public bool DeleteConcept(string token, ProductConcept param)
        {
            // string rest = "";
            if (string.IsNullOrWhiteSpace(restDelete))
            {
                restDelete = getAddress("/REST/DeleteConcept");
            }
            try
            {

               // rest = getAddress("/REST/DeleteConcept");

                mLog.Debug("CALL TO: " + restDelete);

                var request = WebRequest.Create(restDelete);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    if (result.Equals("true"))
                        return true;
                    else
                        return false;
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + restDelete, e);
                return false;
            }
        }

        public bool DeleteConceptList(string token, List<ProductConcept> param)
        {
            // string rest = "";
            if (string.IsNullOrWhiteSpace(restDelete))
            {
                restDelete = getAddress("/REST/DeleteConceptList");
            }
            try
            {

                // rest = getAddress("/REST/DeleteConcept");

                mLog.Debug("CALL TO: " + restDelete);

                var request = WebRequest.Create(restDelete);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    if (result.Equals("true"))
                        return true;
                    else
                        return false;
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + restDelete, e);
                return false;
            }
        }

        public bool DeleteAllConceptOnProduct(string token, List<ProductConcept> param)
        {
            // string rest = "";
            if (string.IsNullOrWhiteSpace(restDelete))
            {
                restDelete = getAddress("/REST/DeleteAllConceptOnProduct");
            }
            try
            {

                // rest = getAddress("/REST/DeleteConcept");

                mLog.Debug("CALL TO: " + restDelete);

                var request = WebRequest.Create(restDelete);
                request.Method = "POST";
                request.ContentType = "application/json";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(param));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    if (result.Equals("true"))
                        return true;
                    else
                        return false;
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + restDelete, e);
                return false;
            }
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("ConceptSearchSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("ConceptSearchSvc") + rest_function;
        }
        private string getGenericSvcAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("GenericTableSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("GenericTableSvc") + rest_function;
        }
    }
}
