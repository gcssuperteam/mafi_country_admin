﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;

namespace GISClientLib.Models
{
    public class DeliverNoteModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DeliverNoteModel(string token, bool use_token_call)
            : base(token, use_token_call)
        {

        }

        public GIS_Dto.DeliverNoteOH GetDeliverNoteById(string id, bool with_rows)
        {
            string rest = "";
            DeliverNoteOH doh = null;

            try
            {

                if (!string.IsNullOrEmpty(id))
                {
                    rest = getAddress("/REST/GetDeliverNoteById") + @"/" + id + @"/" + with_rows.ToString();

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    doh = JsonConvert.DeserializeObject<DeliverNoteOH>(response);
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return doh;

        }

        public GIS_Dto.DeliverNoteOH GetDeliverNoteByOrderId(string id, bool with_rows)
        {
            string rest = "";
            DeliverNoteOH doh = null;

            try
            {

                if (!string.IsNullOrEmpty(id))
                {
                    rest = getAddress("/REST/GetDeliverNoteByOrderId") + @"/" + id + @"/" + with_rows.ToString();

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    doh = JsonConvert.DeserializeObject<DeliverNoteOH>(response);
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return doh;

        }

        public string GetLastOpenDeliverNoteForOrder(string id)
        {
            string rest = "";
            SingleResult<string> result = null;

            try
            {

                if (!string.IsNullOrEmpty(id))
                {
                    rest = getAddress("/REST/GetLastOpenDeliverNoteForOrder") + @"/" + id;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    result = JsonConvert.DeserializeObject<SingleResult<string>>(response);
                }

            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return result.Result;

        }

        public List<DeliverNoteRow> GetDeliverNoteRowList(string id)
        {
            string rest = "";

            List<DeliverNoteRow> lst = new List<DeliverNoteRow>();

            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    rest = getAddress("/REST/GetDeliverNoteRowList") + @"/" + id;

                    mLog.Debug("CALL TO: " + rest);

                    WebRequest request = WebRequest.Create(rest);
                    WebResponse ws = request.GetResponse();
                    Encoding enc = System.Text.Encoding.GetEncoding(1252);
                    StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                    string response = responseStream.ReadToEnd();
                    responseStream.Close();

                    lst = JsonConvert.DeserializeObject<List<DeliverNoteRow>>(response);
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }

            return lst;
        }

        public void updateDeliverNoteHead(DeliverNoteOH doh)
        {
            string rest = "";
            try
            {
                rest = getAddress("/REST/UpdateDeliveryNoteHead");
                mLog.Debug("CALL TO: " + rest);

                var request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(doh));
                }

                using (var response = request.GetResponse())

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                    // do something with the results
                }
            }
            catch (Exception e)
            {
                mLog.Error("Error in call: " + rest, e);
            }
        }
        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("DeliverNoteSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("DeliverNoteSvc") + rest_function;
        }
    }
}
