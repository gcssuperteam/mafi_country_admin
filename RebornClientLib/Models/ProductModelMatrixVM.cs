﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GIS_Dto;

namespace GISClientLib.Models
{
    public class ProductModelMatrixVM : BaseModel
    {
        public GIS_Dto.ProductModel product { get; set; }
        public string selectedWarehouse { get; set; }
    }
}
