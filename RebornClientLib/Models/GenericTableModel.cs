﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using GIS_Dto;
using System.Threading;
using System.Threading.Tasks;

namespace GISClientLib.Models
{
    public class GenericTableModel : BaseModel
    {
        private static readonly log4net.ILog mLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/GetTableValueByIndex/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //SingleResult<FieldObject> GetTableValueByIndex(string token, GetTableValueRequest request);

        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/UpdateTableByIndex/{token}/{insert}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //UpdateResult UpdateTableByIndex(string token, UpdateTableIndexObject updateObj, string insert);

        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/UpdateTableByRange/{token}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //UpdateResult UpdateTableByRange(string token, UpdateTableRangeObject updateObj);



        public GenericTableModel(string token, bool use_token_call) : base(token, use_token_call)
        {
        }

        public SingleResult<FieldObject> GetTableValueByIndex(string uid, GetTableValueRequest tabreq)
        {
            SingleResult<FieldObject> result = new SingleResult<FieldObject>();
            string rest = "";

            try
            {
                rest = getAddress("/REST/GetTableValueByIndex");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(tabreq));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<SingleResult<FieldObject>>(response);
            }
            catch(Exception e)
            {
                mLog.Error("Error in GetTableValueByIndex", e);
            }

            return result;
        }

        public GeneralTableUpdateResult UpdateTableByIndex(string uid, UpdateTableIndexObject updateObj, string insert)
        {
            GeneralTableUpdateResult result = new GeneralTableUpdateResult();
            string rest = "";

            try
            {
                rest = getAddress("/REST/UpdateTableByIndex") + @"/" + insert;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(updateObj));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<GeneralTableUpdateResult>(response);
                
            }
            catch (Exception e)
            {
                mLog.Error("Error in UpdateTableByIndex", e);
            }

            return result;
        }

        public List<GeneralTableUpdateResult> UpdateTableByIndexList(string uid, List<UpdateTableIndexObject> updateObj, string insert)
        {
            List<GeneralTableUpdateResult> result = new List<GeneralTableUpdateResult>();
            string rest = "";

            try
            {
                rest = getAddress("/REST/UpdateTableByIndexList") + @"/" + insert;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";
                request.Timeout = Timeout.Infinite;

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(updateObj));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<List<GeneralTableUpdateResult>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in UpdateTableByIndex", e);
            }

            return result;
        }

        public List<GeneralTableUpdateResult> UpdateTableByIndexListAsync(string uid, List<UpdateTableIndexObject> updateObj, string insert)
        {
            List<GeneralTableUpdateResult> result = new List<GeneralTableUpdateResult>();
            string rest = "";

            try
            {
                rest = getAddress("/REST/UpdateTableByIndexList") + @"/" + insert;

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";
                request.Timeout = Timeout.Infinite;
                

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(updateObj));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<List<GeneralTableUpdateResult>>(response);
            }
            catch (Exception e)
            {
                mLog.Error("Error in UpdateTableByIndex", e);
            }

            return result;
        }

        public UpdateResult UpdateTableByRange(string uid, UpdateTableRangeObject updateObj)
        {
            UpdateResult result = new UpdateResult();
            string rest = "";

            try
            {
                rest = getAddress("/REST/UpdateTableByRange");

                mLog.Debug("CALL TO: " + rest);

                WebRequest request = WebRequest.Create(rest);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                if (!string.IsNullOrEmpty(Token))
                {
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Token + ":" + "password"));
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(JsonConvert.SerializeObject(updateObj));
                }

                WebResponse ws = request.GetResponse();
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                string response = responseStream.ReadToEnd();
                responseStream.Close();

                result = JsonConvert.DeserializeObject<UpdateResult>(response);

            }
            catch (Exception e)
            {
                mLog.Error("Error in UpdateTableByRange", e);
            }

            return result;
        }

        private string getAddress(string rest_function)
        {
            if (UseTokenCall)
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("GenericTableSvc") + rest_function + "/" + Token;
            else
                return Common.CommonFunctions.getRebornBaseAddress() + Common.CommonFunctions.getRebornEndpointAddress("GenericTableSvc") + rest_function;
        }

    }
}
