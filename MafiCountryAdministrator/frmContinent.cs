﻿using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MafiCountryAdministrator
{
    public partial class frmContinent : Form
    {
        public frmContinent()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception e)
            {
                Log.Error(e, "Error initializing continent");
            }
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //CountryFunc.AddContinent(cb.Text, textBox1.Text);
                MessageBox.Show("Kontinent sparad!");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when clicking save continent");
            }
        }
    }
}
