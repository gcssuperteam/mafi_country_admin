﻿using MafiCountryAdministrator.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Serilog;
using System.IO;

namespace MafiCountryAdministrator
{
    public partial class frmCounty : Form
    {
        private bool AddNew { get; set; } = false;
        public frmCounty()
        {
            try
            {
                string logDir = Properties.Settings.Default.Log + "\\log.txt";
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo.Console()
                    .WriteTo.File(logDir, rollingInterval: RollingInterval.Month)
                    .CreateLogger();

                InitializeComponent();
                List<string> tokens = GetCompanyList();
                if (tokens != null)
                {
                    //Log.Debug("Number of tokens found: '" + tokens.Count + "'");
                    foreach (string token in tokens)
                    {
                        comboBoxCompanies.Items.Add(token);
                    }
                    comboBoxCompanies.SelectedIndex = 0;
                }
                else
                {
                    Log.Error("Error when adding a token to the company list");
                    comboBoxCompanies.Items.Add("ERROR");
                }

                CountryFunc.Token = tokens[0]; //comboBoxCompanies.Text;
                ContinentFunc.Token = tokens[0]; //comboBoxCompanies.Text;
                
                // Region combobox
                List<RegionDTO> regionDTOs = CountryFunc.GetRegions();
                Dictionary<int, string> regions = new Dictionary<int, string>();
                regions.Add(0, "");
                foreach (RegionDTO region in regionDTOs)
                {
                    regions.Add(int.Parse(region.Id), region.Name);
                }
                comboBoxRegion.DataSource = regions.ToArray();
                comboBoxRegion.DisplayMember = "Value";
                comboBoxRegion.ValueMember = "Key";

                // Date formats
                comboBoxDateFormat.DataSource = CountryFunc.GetDateFormats().ToArray();
                comboBoxDateFormat.DisplayMember = "Value";
                comboBoxDateFormat.ValueMember = "Key";

                
                
                refreshList();
                refreshcontinentList();
                cboCountryList.SelectedIndex = 0;
                DisplayCountryDetails(getCurrentId());

                Log.Information("MafiCountryAdministrator initiated: '" + DateTime.Now + "'. The number of tokens added is: '" + tokens.Count + "' with the selected token: '" + comboBoxCompanies.Text + "'");
            }
            catch (Exception e)
            {
                Log.Error(e, "Error when initiating MafiCountryAdministrator");
            }
        }

        private void DisplayCountryDetails(string countryId)
        {
            try
            {
                if (cboCountryList.SelectedItem != null)
                {
                    CountryDTO country = CountryFunc.GetCountry(countryId);
                    
                    txtName.Text = country.Name;
                    txtLocalName.Text = country.LocalName;

                    if (CountryFunc.Token.Equals("mafi") || CountryFunc.Token.Equals("mafiNorge"))
                    {
                        checkboxBelongsTo.Text = "Tillhör EU";
                        checkboxBelongsTo.Checked = country.IsEU;
                    }
                    else if (CountryFunc.Token.Equals("mafiUSA"))
                    {
                        checkboxBelongsTo.Text = "Tillhör USA";
                        checkboxBelongsTo.Checked = country.IsUS;
                    }
                    else if (CountryFunc.Token.Equals("mafiCN"))
                    {
                        checkboxBelongsTo.Text = "Tillhör Kina";
                        checkboxBelongsTo.Checked = true;
                    }
                    else
                    {
                        checkboxBelongsTo.Text = "Tillhör EU";
                        checkboxBelongsTo.Checked = country.IsEU;
                    }

                    comboBoxDateFormat.SelectedIndex = country.DateFormat;

                    if (!string.IsNullOrEmpty(country.Region))
                    {
                        if (int.TryParse(country.Region, out int region))
                        {
                            if (region > 4)
                            {
                                region = 0;
                            }
                            comboBoxRegion.SelectedIndex = region;
                        }
                    }
                    else
                    {
                        // if blank then point to index 0
                        comboBoxRegion.SelectedIndex = 0;
                    }

                    
                    textBoxTax1.Text = country.Tax1.ToString();
                    textBoxTax2.Text = country.Tax2.ToString();
                    textBoxTax3.Text = country.Tax3.ToString();
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in DisplayCountryDetails");
            }
        }

        private List<string> GetCompanyList()
        {
            try
            {
                string inFile = AppDomain.CurrentDomain.BaseDirectory + "GarpCompanies.txt";
                StreamReader file = new StreamReader(@inFile);
                string fileRow;
                List<string> result = new List<string>();
                while ((fileRow = file.ReadLine()) != null)
                {
                    // Assume the file format "B24#123456789" ie B24#MESSAGEID
                    if (!string.IsNullOrEmpty(fileRow.Trim()))
                    {
                        if (!fileRow.Substring(0, 1).Equals("#")) // Comments
                        {
                            result.Add(fileRow.Trim());
                        }
                    }
                }
                file.Close();
                return result;
            }
            catch (Exception e)
            {
                Log.Error("Error in GetCompanyList", e);
                return null;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            //using (StreamWriter sw = File.AppendText(AppDomain.CurrentDomain.BaseDirectory + "settings\\b24\\OrderMessageIds.txt"))
            //{
            //    sw.WriteLine("B24#" + order.MessageId);
            //}
        }

        private void cboCountryList_DropDown(object sender, EventArgs e)
        {
            try
            {
                //List<CountryDTO> countryList = CountryFunc.GetCountryList();

                //cboCountryList.Items.Clear();
                //foreach (CountryDTO c in countryList)
                //{
                //    cboCountryList.Items.Add(c.Id + " - " + c.Name);
                //}
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when populating the country drop down list");
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ClearCountryForm();
                AddNew = true;
                cboCountryList.DropDownStyle = ComboBoxStyle.DropDown;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when clicking the Add button");
            }
        }

        private void ClearCountryForm()
        {
            try
            {
                //cboCountryList.DropDownStyle = ComboBoxStyle.DropDown;
                cboCountryList.Focus();
                cboCountryList.Text = "";

                txtName.Text = "";
                txtLocalName.Text = "";
                checkboxBelongsTo.Checked = false;
                comboBoxDateFormat.SelectedIndex = 0;
                comboBoxRegion.SelectedIndex = 0;
                textBoxTax1.Text = "0";
                textBoxTax2.Text = "0";
                textBoxTax3.Text = "0";
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in ClearCountryForm");
            }
        }


        private string getCurrentId()
        {
            try
            {
                string id = "";

                if (cboCountryList.SelectedItem != null)
                {
                    id = cboCountryList.SelectedItem.ToString();

                    if (id.Contains("-"))
                    {
                        id = id.Substring(0, id.IndexOf("-")).Trim();
                    }
                }
                else if (cboCountryList.Text != "")
                {
                    id = cboCountryList.Text;
                }

                return id.Trim();
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in getCurrentId");
                return null;
            }
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string countryFullName = cboCountryList.SelectedText;//(string) cboCountryList.SelectedValue;
                DialogResult dialogResult = MessageBox.Show("Vill du spara " + getCurrentId() + "?", "Tilel", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    CountryDTO country = GetCurrentCountry();
                    //country.Id = getCurrentId();
                    //country.Name = txtName.Text;
                    //country.LocalName = txtLocalName.Text;
                    //country.IsEU = checkboxBelongsTo.Checked;
                    ////country.IsUS = checkBoxUS.Checked;
                    //country.DateFormat = comboBoxDateFormat.SelectedIndex;
                    //country.Region = comboBoxRegion.SelectedIndex.ToString();
                    ////country.Continent = cboContinentList.SelectedItem.ToString().Substring(0, 1);

                    if (!string.IsNullOrEmpty(country.Id))
                    {
                        if (AddNew) //cboCountryList.DropDownStyle == ComboBoxStyle.DropDown)
                        {
                            CountryFunc.AddCountry(country);

                            //cboCountryList.DropDownStyle = ComboBoxStyle.DropDownList;
                            cboCountryList.Focus();

                            cboCountryList.Items.Add(country.Id);
                            //cboCountryList.SelectedIndex = cboCountryList.Items.IndexOf(country.Id);
                            countryFullName = country.Id + " - " + country.Name;
                        }
                        else
                        {
                            CountryFunc.UpdateCountry(country);
                        }
                        
                        refreshList();
                        DisplayCountryDetails(country.Id);
                        countryFullName = country.Id + " - " + country.Name;
                        cboCountryList.SelectedIndex = cboCountryList.Items.IndexOf(countryFullName);
                    }
                }
                AddNew = false;
                cboCountryList.DropDownStyle = ComboBoxStyle.DropDownList;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when clicking the save button");
            }
        }

        private int GetCurrentCountryListIndex(string countryCode)
        {
            try
            {
                int result = -1;

                int count = cboCountryList.Items.Count;
                for (int i = 0; i <= (count - 1); i++)
                {
                    cboCountryList.SelectedIndex = i;
                    string value = (string)cboCountryList.SelectedValue;
                    
                }

                return result;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetCurrentCountryListIndex");
                return -1;
            }
        }

        private CountryDTO GetCurrentCountry()
        {
            try
            {
                CountryDTO result = new CountryDTO()
                {
                    Id = getCurrentId(),
                    Name = txtName.Text,
                    LocalName = txtLocalName.Text,
                    IsEU = checkboxBelongsTo.Text.Equals("Tillhör EU") ? checkboxBelongsTo.Checked : false,
                    IsUS = checkboxBelongsTo.Text.Equals("Tillhör USA") ? checkboxBelongsTo.Checked : false,
                    Region = comboBoxRegion.SelectedIndex.ToString(),
                    DateFormat = comboBoxDateFormat.SelectedIndex,
                    Tax1 = decimal.Parse(textBoxTax1.Text),
                    Tax2 = decimal.Parse(textBoxTax2.Text),
                    Tax3 = decimal.Parse(textBoxTax3.Text)
                };
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetCurrentCountry");
                return null;
            }
        }

        private void cboCountryList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DisplayCountryDetails(getCurrentId());
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when the selected country changed");
            }
        }

        //private int ComboBoxDateFormat( string dateFormat )
        //{
        //    int result = -1;
        //    if 




        //    return result;
        //}




        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Vill du radera " + getCurrentId() + "?", "Radera", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    CountryFunc.DeleteCountry(getCurrentId());
                }
                else
                {
                    MessageBox.Show("Fegis");
                }
                refreshList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when clicking the delete button");
            }
        }

        private void txtLocalName_TextChanged(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when text changed for the local name");
            }
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when text changed for the Swedish name");
            }
        }

        private void frmCounty_Load(object sender, EventArgs e)
        {
            try
            {
                //refreshList();
                //refreshcontinentList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when text changed for the form loaded");
            }
        }

        private void refreshList()
        {
            try
            {
                List<CountryDTO> countryList = CountryFunc.GetCountryList();
                //Log.Debug("Number of countries found: '" + countryList.Count + "'");
                cboCountryList.Items.Clear();
                foreach (CountryDTO c in countryList)
                {
                    cboCountryList.Items.Add(c.Id + " - " + c.Name);
                }
                cboCountryList.SelectedItem = getCurrentId();
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in refreshList");
            }
        }

        private void refreshcontinentList()
        {
            try
            {
                List<RegionDTO> list = ContinentFunc.GetContientList();

                //cboContinentList.Items.Clear();
                //foreach (ContinentDTO c in list)
                //{
                //    cboContinentList.Items.Add(c.Id + " - " + c.Name);
                //    cboContinentList.SelectedIndex = 0;
                //}
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in refreshcontinentList");
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void openContinent_Click(object sender, EventArgs e)
        {
            try
            {
                frmContinent continent = new frmContinent();
                {


                    continent.ShowDialog();



                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when clicking open continent");
            }
        }

        private void comboBoxCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CountryFunc.Token = comboBoxCompanies.Text;
                ContinentFunc.Token = comboBoxCompanies.Text;
                refreshList();
                ClearCountryForm();
                cboCountryList.SelectedIndex = 0;
                DisplayCountryDetails(getCurrentId());
                refreshcontinentList();
                Log.Information("Company token successfully changed to: '" + comboBoxCompanies.Text + "'");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error when changing Garp company");
            }
        }

        //private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}

        //private void cboContinentList_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    MessageBox.Show(cboContinentList.SelectedItem.ToString().Substring(0,1));




    }
}

