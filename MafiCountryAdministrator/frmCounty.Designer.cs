﻿namespace MafiCountryAdministrator
{
    partial class frmCounty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCounty));
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxDateFormat = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtLocalName = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.cboCountryList = new System.Windows.Forms.ComboBox();
            this.checkboxBelongsTo = new System.Windows.Forms.CheckBox();
            this.comboBoxCompanies = new System.Windows.Forms.ComboBox();
            this.labeRegion = new System.Windows.Forms.Label();
            this.comboBoxRegion = new System.Windows.Forms.ComboBox();
            this.textBoxTax1 = new System.Windows.Forms.TextBox();
            this.textBoxTax2 = new System.Windows.Forms.TextBox();
            this.textBoxTax3 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(94, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Landkod:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // comboBoxDateFormat
            // 
            this.comboBoxDateFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDateFormat.FormattingEnabled = true;
            this.comboBoxDateFormat.Location = new System.Drawing.Point(153, 215);
            this.comboBoxDateFormat.Name = "comboBoxDateFormat";
            this.comboBoxDateFormat.Size = new System.Drawing.Size(121, 21);
            this.comboBoxDateFormat.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Namn på svenska:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Namn på eget språk:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(76, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Datumformat:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(153, 101);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(142, 20);
            this.txtName.TabIndex = 8;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // txtLocalName
            // 
            this.txtLocalName.Location = new System.Drawing.Point(153, 142);
            this.txtLocalName.Name = "txtLocalName";
            this.txtLocalName.Size = new System.Drawing.Size(142, 20);
            this.txtLocalName.TabIndex = 9;
            this.txtLocalName.TextChanged += new System.EventHandler(this.txtLocalName_TextChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(35, 312);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 11;
            this.btnAdd.Text = "Lägg till";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(115, 312);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Spara";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(199, 312);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Radera";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click_1);
            // 
            // cboCountryList
            // 
            this.cboCountryList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCountryList.FormattingEnabled = true;
            this.cboCountryList.Location = new System.Drawing.Point(153, 68);
            this.cboCountryList.Name = "cboCountryList";
            this.cboCountryList.Size = new System.Drawing.Size(142, 21);
            this.cboCountryList.TabIndex = 14;
            this.cboCountryList.DropDown += new System.EventHandler(this.cboCountryList_DropDown);
            this.cboCountryList.SelectedIndexChanged += new System.EventHandler(this.cboCountryList_SelectedIndexChanged);
            // 
            // checkboxBelongsTo
            // 
            this.checkboxBelongsTo.AutoSize = true;
            this.checkboxBelongsTo.Location = new System.Drawing.Point(153, 181);
            this.checkboxBelongsTo.Name = "checkboxBelongsTo";
            this.checkboxBelongsTo.Size = new System.Drawing.Size(15, 14);
            this.checkboxBelongsTo.TabIndex = 15;
            this.checkboxBelongsTo.UseVisualStyleBackColor = true;
            // 
            // comboBoxCompanies
            // 
            this.comboBoxCompanies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCompanies.FormattingEnabled = true;
            this.comboBoxCompanies.Location = new System.Drawing.Point(347, 312);
            this.comboBoxCompanies.Name = "comboBoxCompanies";
            this.comboBoxCompanies.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCompanies.TabIndex = 16;
            this.comboBoxCompanies.SelectedIndexChanged += new System.EventHandler(this.comboBoxCompanies_SelectedIndexChanged);
            // 
            // labeRegion
            // 
            this.labeRegion.AutoSize = true;
            this.labeRegion.Location = new System.Drawing.Point(102, 254);
            this.labeRegion.Name = "labeRegion";
            this.labeRegion.Size = new System.Drawing.Size(44, 13);
            this.labeRegion.TabIndex = 18;
            this.labeRegion.Text = "Region:";
            // 
            // comboBoxRegion
            // 
            this.comboBoxRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRegion.FormattingEnabled = true;
            this.comboBoxRegion.Location = new System.Drawing.Point(153, 251);
            this.comboBoxRegion.Name = "comboBoxRegion";
            this.comboBoxRegion.Size = new System.Drawing.Size(121, 21);
            this.comboBoxRegion.TabIndex = 17;
            // 
            // textBoxTax1
            // 
            this.textBoxTax1.Location = new System.Drawing.Point(21, 25);
            this.textBoxTax1.Name = "textBoxTax1";
            this.textBoxTax1.Size = new System.Drawing.Size(100, 20);
            this.textBoxTax1.TabIndex = 19;
            // 
            // textBoxTax2
            // 
            this.textBoxTax2.Location = new System.Drawing.Point(21, 51);
            this.textBoxTax2.Name = "textBoxTax2";
            this.textBoxTax2.Size = new System.Drawing.Size(100, 20);
            this.textBoxTax2.TabIndex = 20;
            // 
            // textBoxTax3
            // 
            this.textBoxTax3.Location = new System.Drawing.Point(21, 77);
            this.textBoxTax3.Name = "textBoxTax3";
            this.textBoxTax3.Size = new System.Drawing.Size(100, 20);
            this.textBoxTax3.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxTax1);
            this.groupBox1.Controls.Add(this.textBoxTax3);
            this.groupBox1.Controls.Add(this.textBoxTax2);
            this.groupBox1.Location = new System.Drawing.Point(328, 156);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(140, 111);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Skatter";
            // 
            // frmCounty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 368);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labeRegion);
            this.Controls.Add(this.comboBoxRegion);
            this.Controls.Add(this.comboBoxCompanies);
            this.Controls.Add(this.checkboxBelongsTo);
            this.Controls.Add(this.cboCountryList);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtLocalName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxDateFormat);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCounty";
            this.Text = "Uppdatering Länder";
            this.Load += new System.EventHandler(this.frmCounty_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxDateFormat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtLocalName;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cboCountryList;
        private System.Windows.Forms.CheckBox checkboxBelongsTo;
        private System.Windows.Forms.ComboBox comboBoxCompanies;
        private System.Windows.Forms.Label labeRegion;
        private System.Windows.Forms.ComboBox comboBoxRegion;
        private System.Windows.Forms.TextBox textBoxTax1;
        private System.Windows.Forms.TextBox textBoxTax2;
        private System.Windows.Forms.TextBox textBoxTax3;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

