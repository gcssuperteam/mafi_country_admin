﻿namespace MafiCountryAdministrator
{
    partial class frmCounty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtLocalName = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.cboCountryList = new System.Windows.Forms.ComboBox();
            this.checkboxEU = new System.Windows.Forms.CheckBox();
            this.checkBoxUS = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.openContinent = new System.Windows.Forms.Button();
            this.txtContinent = new System.Windows.Forms.TextBox();
            this.textContinent2 = new System.Windows.Forms.TextBox();
            this.cboContinentList = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Landkod:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "A - yymmdd",
            "B - yy-mm-dd",
            "C - yyyy-mm-dd",
            "D - yy/mm/dd",
            "E - yyyy/mm/dd",
            "F - yy.mm.dd",
            "G - yyyy.mm.dd",
            "H - dd-mm-yy",
            "I - dd-mm-yyyy",
            "J - dd/mm/yy",
            "K - dd/mm/yyyy",
            "L - dd.mm.yy",
            "M - dd.mm.yyyy",
            "N - mm-dd-yy",
            "O - mm-dd-yyyy",
            "P -  mm/dd/yy",
            "Q - mm/dd/yyyy",
            "R - mm.dd.yy",
            "S - mm.dd.yyyy"});
            this.comboBox1.Location = new System.Drawing.Point(167, 249);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(142, 21);
            this.comboBox1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Namn på svenska:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Namn på eget språk:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Tillhör EU:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 252);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Datumformat:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(167, 101);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(142, 20);
            this.txtName.TabIndex = 1;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // txtLocalName
            // 
            this.txtLocalName.Location = new System.Drawing.Point(167, 142);
            this.txtLocalName.Name = "txtLocalName";
            this.txtLocalName.Size = new System.Drawing.Size(142, 20);
            this.txtLocalName.TabIndex = 2;
            this.txtLocalName.TextChanged += new System.EventHandler(this.txtLocalName_TextChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(43, 335);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 11;
            this.btnAdd.Text = "Lägg till";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(136, 335);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Spara";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(234, 335);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Radera";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click_1);
            // 
            // cboCountryList
            // 
            this.cboCountryList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCountryList.FormattingEnabled = true;
            this.cboCountryList.Location = new System.Drawing.Point(167, 65);
            this.cboCountryList.Name = "cboCountryList";
            this.cboCountryList.Size = new System.Drawing.Size(142, 21);
            this.cboCountryList.TabIndex = 0;
            this.cboCountryList.DropDown += new System.EventHandler(this.cboCountryList_DropDown);
            this.cboCountryList.SelectedIndexChanged += new System.EventHandler(this.cboCountryList_SelectedIndexChanged);
            // 
            // checkboxEU
            // 
            this.checkboxEU.AutoSize = true;
            this.checkboxEU.Location = new System.Drawing.Point(167, 182);
            this.checkboxEU.Name = "checkboxEU";
            this.checkboxEU.Size = new System.Drawing.Size(15, 14);
            this.checkboxEU.TabIndex = 3;
            this.checkboxEU.UseVisualStyleBackColor = true;
            // 
            // checkBoxUS
            // 
            this.checkBoxUS.AutoSize = true;
            this.checkBoxUS.Location = new System.Drawing.Point(167, 205);
            this.checkBoxUS.Name = "checkBoxUS";
            this.checkBoxUS.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUS.TabIndex = 4;
            this.checkBoxUS.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(41, 209);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Inom USA:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 297);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Lägg till kontinent:";
            // 
            // openContinent
            // 
            this.openContinent.Location = new System.Drawing.Point(304, 393);
            this.openContinent.Name = "openContinent";
            this.openContinent.Size = new System.Drawing.Size(28, 23);
            this.openContinent.TabIndex = 16;
            this.openContinent.Text = "....";
            this.openContinent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.openContinent.UseVisualStyleBackColor = true;
            this.openContinent.Click += new System.EventHandler(this.openContinent_Click);
            // 
            // txtContinent
            // 
            this.txtContinent.Location = new System.Drawing.Point(167, 296);
            this.txtContinent.Name = "txtContinent";
            this.txtContinent.Size = new System.Drawing.Size(44, 20);
            this.txtContinent.TabIndex = 17;
            // 
            // textContinent2
            // 
            this.textContinent2.Location = new System.Drawing.Point(220, 297);
            this.textContinent2.Name = "textContinent2";
            this.textContinent2.Size = new System.Drawing.Size(89, 20);
            this.textContinent2.TabIndex = 18;
            // 
            // cboContinentList
            // 
            this.cboContinentList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboContinentList.FormattingEnabled = true;
            this.cboContinentList.Items.AddRange(new object[] {
            "A - Australien\t\t",
            "B - Afrika",
            "C -  Sydamerika\t",
            "D - Europa",
            "E - Nordamerika",
            "F - Asien",
            "G - Antaktis\t"});
            this.cboContinentList.Location = new System.Drawing.Point(330, 297);
            this.cboContinentList.Name = "cboContinentList";
            this.cboContinentList.Size = new System.Drawing.Size(142, 21);
            this.cboContinentList.TabIndex = 19;
            this.cboContinentList.SelectedIndexChanged += new System.EventHandler(this.cboContinentList_SelectedIndexChanged);
            // 
            // frmCounty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 453);
            this.Controls.Add(this.cboContinentList);
            this.Controls.Add(this.textContinent2);
            this.Controls.Add(this.txtContinent);
            this.Controls.Add(this.openContinent);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.checkBoxUS);
            this.Controls.Add(this.checkboxEU);
            this.Controls.Add(this.cboCountryList);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtLocalName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label1);
            this.Name = "frmCounty";
            this.Text = "Uppdatering Länder";
            this.Load += new System.EventHandler(this.frmCounty_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtLocalName;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cboCountryList;
        private System.Windows.Forms.CheckBox checkboxEU;
        private System.Windows.Forms.CheckBox checkBoxUS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button openContinent;
        private System.Windows.Forms.TextBox txtContinent;
        private System.Windows.Forms.TextBox textContinent2;
        private System.Windows.Forms.ComboBox cboContinentList;
    }
}

