﻿using MafiCountryAdministrator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GISClientLib;
using GIS_Dto;
using GISClientLib.Models;

namespace MafiCountryAdministrator
{

    public static class ContinentFunc
    {
        private static List<RegionDTO> mCountryList = new List<RegionDTO>();
        public static string Token { get; set; }
        public static List<RegionDTO> GetContientList()
        {
            List<RegionDTO> result = new List<RegionDTO>();
            BaseDataModel bdm = new BaseDataModel(Token, true);

            List<BaseTable> lstCountry = bdm.GetBaseTableList("CON");

            try
            {
                foreach (BaseTable b in lstCountry)
                {
                    RegionDTO country = new RegionDTO
                    {
                        Id = b.Id,
                        Name = b.Description
                    };

                    result.Add(country);
                }
                if (mCountryList.Count == 0)
                {
                    mCountryList = result;
                }

            }
            catch (Exception e)
            {

            }

            return result;
        }

        public static bool DeleteCountry(string id)
        {
            bool result = true;

            try
            {
                BaseDataModel bdm = new BaseDataModel(Token, true);
                BaseTable bt = bdm.GetBaseTable("CON", id);

                if (bt != null)
                {
                    bdm.DeleteBaseTable(bt);
                }

                result = true;
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public static RegionDTO AddCountry(RegionDTO country)
        {
            RegionDTO result = new RegionDTO();
            BaseDataModel bdm = new BaseDataModel(Token, true);

            try
            {
                mCountryList.Add(country);
                BaseTable land = new BaseTable();

                land.Id = country.Id;
                land.Description = country.Name;
         
                land.Type = "CON";
              
                
                bdm.AddBaseTable(land);
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public static RegionDTO UpdateCountry(RegionDTO country)
        {
            RegionDTO result = new RegionDTO();
            BaseDataModel bdm = new BaseDataModel(Token, true);

            try
            {
                //ContinentDTO con = mCountryList.Find(c => c.Id == country.Id);
                //int idx = mCountryList.IndexOf(country);
                //mCountryList[idx] = country;

                BaseTable land = new BaseTable();

                land.Id = country.Id;
                land.Description = country.Name;
            
                bdm.UpdateBaseTable(land);

            }
            catch (Exception e)
            {

            }

            return result;
        }

        private static void Marcus()
        {

        }

        public static RegionDTO GetCountry(string id)
        {
            RegionDTO result = new RegionDTO();
            BaseDataModel bdm = new BaseDataModel(Token, true);

            try
            {
                BaseTable bt = bdm.GetBaseTable("CON", id);

                result.Id = bt.Id;
                result.Name = bt.Description;

            }
            catch (Exception e)
            {

            }

            return result;
        }
        public static string GetContinentdescription(string id)
        {
            string result = "";
            BaseDataModel bdm = new BaseDataModel(Token, true);

            try
            {
                BaseTable bt = bdm.GetBaseTable("CON", id);


                result = bt.Description;

            }
            catch (Exception e)
            {

            }

            return result;
        }
        public static void AddContinent(string id, string name)
        {

            BaseDataModel bdm = new BaseDataModel(Token, true);

            BaseTable continent = new BaseTable();

            continent.Id = id;
            continent.Description = name;

            continent.Type = "CON";

            bdm.AddBaseTable(continent);


        }

    }
}