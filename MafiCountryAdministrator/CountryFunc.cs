﻿using MafiCountryAdministrator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GISClientLib;
using GIS_Dto;
using GISClientLib.Models;
using Serilog;

namespace MafiCountryAdministrator
{
    public static class CountryFunc
    {
        private static List<CountryDTO> mCountryList = new List<CountryDTO>();
        public static string Token { get; set; } 
        public static List<CountryDTO> GetCountryList()
        {
            Log.Debug("'" + Token + "' is used as token.");
            List<CountryDTO> result = new List<CountryDTO>();
            BaseDataModel bdm = new BaseDataModel(Token, true);
            
            List<BaseTable> lstCountry = bdm.GetBaseTableList("11");
            //Log.Debug("Number of countries found: '" + lstCountry.Count + "'");
            try
            {
                foreach(BaseTable b in lstCountry)
                {
                    CountryDTO country = new CountryDTO
                    {
                        Id = b.Id,
                        Name = b.Description,
                        LocalName = b.Description2,
                        DateFormat = GetDateFormatNumber(b.Code2),
                        IsEU = b.Code1.Equals("E"),
                        IsUS = b.Code3.Equals("U"),
                        Region = b.Code12,
                        Tax1 = decimal.Parse(b.Num1.ToString()) / 100,
                        Tax2 = decimal.Parse(b.Num2.ToString()) / 100,
                        Tax3 = decimal.Parse(b.Num3.ToString()) / 100
                    };

                    result.Add(country);
                }
                if(mCountryList.Count == 0)
                {
                    Log.Error("ZERO countries found!");
                    mCountryList = result;
                }
                
            }
            catch(Exception e)
            {
                Log.Error(e, "Error in GetCountryList");
            }

            return result;
        }

        public static List<RegionDTO> GetRegions()
        {
            try
            {
                List<RegionDTO> result = new List<RegionDTO>()
                {
                    new RegionDTO()
                    {
                        Id = "1",
                        Name = "Europe"
                    },
                    new RegionDTO()
                    {
                        Id = "2",
                        Name = "Americas"
                    },
                    new RegionDTO()
                    {
                        Id = "3",
                        Name = "Asia"
                    },
                    new RegionDTO()
                    {
                        // Mafi has "5" as this region but we need a 4 as the 4th index
                        Id = "5",
                        Name = "Africa and the Middle East"
                    }
                };
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetRegions");
                return null;
            }
        }

        public static bool DeleteCountry(string id)
        {
            bool result = true;

            try
            {
                BaseDataModel bdm = new BaseDataModel(Token, true);
                BaseTable bt = bdm.GetBaseTable("11", id);
             
                if(bt != null)
                {
                    bdm.DeleteBaseTable(bt);
                }

                result = true;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in DeleteCountry");
            }

            return result;
        }

        public static CountryDTO AddCountry(CountryDTO country)
        {
            CountryDTO result = new CountryDTO();
            BaseDataModel bdm = new BaseDataModel(Token, true);

            try
            {
                mCountryList.Add(country);
                BaseTable land = new BaseTable();

                country.Tax1 *= 100;
                country.Tax2 *= 100;
                country.Tax3 *= 100;

                land.Id = country.Id;
                land.Description = country.Name;
                land.Description2 = country.LocalName;
                land.Type = "11";
                land.Code12 = country.Region;
                land.Code2 = GetDateFormatString(country.DateFormat);
                land.Num1 = int.Parse(country.Tax1.ToString());
                land.Num2 = int.Parse(country.Tax2.ToString());
                land.Num3 = int.Parse(country.Tax3.ToString());

                if (country.IsEU == true)
                {
                    land.Code1 = "E";

                }
                else
                {
                    land.Code1 = null;
                }

                if (country.IsUS == true)
                {
                    land.Code3 = "U";
                }
                else
                {
                    land.Code3 = null;
                }
                bdm.AddBaseTable(land);
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in AddCountry");
            }

            return result;
        }

        public static CountryDTO UpdateCountry(CountryDTO country)
        {
            CountryDTO result = new CountryDTO();
            BaseDataModel bdm = new BaseDataModel(Token, true);

            try
            {
                //CountryDTO con = mCountryList.Find(c => c.Id == country.Id);
                //int idx = mCountryList.IndexOf(country);
                //mCountryList[idx] = country;
                country.Tax1 *= 100;
                country.Tax2 *= 100;
                country.Tax3 *= 100;
                BaseTable land = new BaseTable()
                {
                    Id = country.Id,
                    Description = country.Name,
                    Description2 = country.LocalName,
                    Type = "11",
                    Code1 = country.IsEU ? "E" : null,
                    Code2 = GetDateFormatString(country.DateFormat),
                    Code3 = country.IsUS ? "U" : null,
                    Code12 = country.Region,
                    Num1 = int.Parse(country.Tax1.ToString()),
                    Num2 = int.Parse(country.Tax2.ToString()),
                    Num3 = int.Parse(country.Tax3.ToString())
                };

                bdm.UpdateBaseTable(land);
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in UpdateCountry");
            }

            return result;
        }

        public static CountryDTO GetCountry(string id)
        {
            CountryDTO result = new CountryDTO();
            BaseDataModel bdm = new BaseDataModel(Token, true);
            
            try
            {
                BaseTable bt = bdm.GetBaseTable("11", id);

                result.Id = bt.Id;
                result.Name = bt.Description;
                result.LocalName = bt.Description2;
                result.IsEU = bt.Code1 == "E" ? true : false;
                result.IsUS = bt.Code3 == "U" ? true : false;
                result.DateFormat = GetDateFormatNumber(bt.Code2);
                result.Region = bt.Code12;
                result.Tax1 = decimal.Parse(bt.Num1.ToString()) / 100;
                result.Tax2 = decimal.Parse(bt.Num2.ToString()) / 100;
                result.Tax3 = decimal.Parse(bt.Num3.ToString()) / 100;

                if (!string.IsNullOrEmpty(result.Region))
                {
                    // The number in code 12 is later used in the index of
                    // the combo box for Region
                    // Africa and the Middle East is the 4th region
                    if (result.Region.Equals("5"))
                    {
                        result.Region = "4";
                    }
                }
                else
                {
                    // If the code is blank then the combo box should point to the blank index 0
                    result.Region = "0";
                }
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetCountry");
            }

            return result;
        }
        public static string GetContinentdescription(string id)
        {
            string result = "";
            BaseDataModel bdm = new BaseDataModel(Token, true);

            try
            {
                BaseTable bt = bdm.GetBaseTable("CON", id);

                
                result = bt.Description;
               
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetContinentdescription");
            }

            return result;
        }
        public static void AddContinent(string id, string name)
        {
           
            BaseDataModel bdm = new BaseDataModel(Token, true);

              BaseTable continent = new BaseTable();

                continent.Id = id;
                continent.Description = name;

                continent.Type = "CON";

                bdm.AddBaseTable(continent);
               
        }

        private static int GetDateFormatNumber(string dateformat)
        {
            try
            {
                int result = -1;
                switch(dateformat)
                {
                    case "": result = 0; break;
                    case "A": result = 1; break;
                    case "B": result = 2; break;
                    case "C": result = 3; break;
                    case "D": result = 4; break;
                    case "E": result = 5; break;
                    case "F": result = 6; break;
                    case "G": result = 7; break;
                    case "H": result = 8; break;
                    case "I": result = 9; break;
                    case "J": result = 10; break;
                    case "K": result = 11; break;
                    case "L": result = 12; break;
                    case "M": result = 13; break;
                    case "N": result = 14; break;
                    case "O": result = 15; break;
                    case "P": result = 16; break;
                    case "Q": result = 17; break;
                    case "R": result = 18; break;
                    case "S": result = 19; break;
                    default: result = -1; break;
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetDateFormatNumber");
                return -1;
            }
        }

        public static Dictionary<int, string> GetDateFormats()
        {
            try
            {
                Dictionary<int, string> dateFormats = new Dictionary<int, string>();
                dateFormats.Add(0, "");
                dateFormats.Add(1, "yymmdd");
                dateFormats.Add(2, "yy-mm-dd");
                dateFormats.Add(3, "yyyy-mm-dd");
                dateFormats.Add(4, "yy/mm/dd");
                dateFormats.Add(5, "yyyy/mm/dd");
                dateFormats.Add(6, "yy.mm.dd");
                dateFormats.Add(7, "yyyy.mm.dd");
                dateFormats.Add(8, "dd-mm-yy");
                dateFormats.Add(9, "dd-mm-yyyy");
                dateFormats.Add(10, "dd/mm/yy");
                dateFormats.Add(11, "dd/mm/yyyy");
                dateFormats.Add(12, "dd.mm.yy");
                dateFormats.Add(13, "dd.mm.yyyy");
                dateFormats.Add(14, "mm-dd-yy");
                dateFormats.Add(15, "mm-dd-yyyy");
                dateFormats.Add(16, "mm/dd/yy");
                dateFormats.Add(17, "mm/dd/yyyy");
                dateFormats.Add(18, "mm.dd.yy");
                dateFormats.Add(19, "mm.dd.yyyy");
                return dateFormats;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetDateFormats");
                return null;
            }
        }

        private static string GetDateFormatString(int dateformat)
        {
            try
            {
                string result = "";
                switch (dateformat)
                {
                    case 0: result = ""; break;
                    case 1: result = "A"; break;
                    case 2: result = "B"; break;
                    case 3: result = "C"; break;
                    case 4: result = "D"; break;
                    case 5: result = "E"; break;
                    case 6: result = "F"; break;
                    case 7: result = "G"; break;
                    case 8: result = "H"; break;
                    case 9: result = "I"; break;
                    case 10: result = "J"; break;
                    case 11: result = "K"; break;
                    case 12: result = "L"; break;
                    case 13: result = "M"; break;
                    case 14: result = "N"; break;
                    case 15: result = "O"; break;
                    case 16: result = "P"; break;
                    case 17: result = "Q"; break;
                    case 18: result = "R"; break;
                    case 19: result = "S"; break;
                    default: result = null; break;
                }
                return result;
            }
            catch (Exception e)
            {
                Log.Error(e, "Error in GetDateFormatString");
                return null;
            }
        }
    }
}
