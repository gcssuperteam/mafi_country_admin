﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MafiCountryAdministrator.Models
{
    public partial class CountryDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string LocalName { get; set; }
        public bool IsEU { get; set; }
        public bool IsUS { get; set; }        
        public int DateFormat { get; set; }
        public string Region { get; set; }
        public decimal Tax1 { get; set; }
        public decimal Tax2 { get; set; }
        public decimal Tax3 { get; set; }
    }

    public partial class RegionDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}


